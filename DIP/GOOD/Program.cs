﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.GOOD
{
    class Program
    {
        static void Main(string[] args)
        {
            XMLdatabase db = new XMLdatabase();
            db.Process("database=Database;server=localhost;User ID=root;Password=;");

            XMLurl url = new XMLurl();
            url.Process("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");

            XMLFile file = new XMLFile();
            file.Process("test.xml");
        }
    }
}
