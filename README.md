# SOLID

Single-responsibility Principle - Open-closed Principle - Liskov substitution principle - Interface segregation principle - Dependency Inversion principle


## Single-responsibility Principle

![Screenshot](s.png)

## Open-closed Principle

![Screenshot](o.png)

## Liskov substitution principle

![Screenshot](l.png)

## Interface segregation principle

![Screenshot](i.png)

## Dependency Inversion principle

![Screenshot](d.png)
