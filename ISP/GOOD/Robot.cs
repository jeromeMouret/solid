﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISP
{
    public class Robot : IEmployer
    {
        public Robot(string Nom)
        {
            this.Nom = Nom;

        }

        public string Nom
        {
            get => default(string);
            set
            {
            }
        }

        public void Travailler()
        {
            Console.WriteLine("Je suis un robot et je travaille.");
        }
    }
}