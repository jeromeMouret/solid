﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISP
{
    public interface IEmployer
    {
        string Nom { get; set; }

        void Travailler();
    }
}