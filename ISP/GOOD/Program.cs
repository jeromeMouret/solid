﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISP.GOOD
{
    class Program
    {
        static void Main(string[] args)
        {

            Femme f = new Femme("Julie");
            f.Travailler();
            f.Manger();
            f.Dormir();

            Homme h = new Homme("Marc");
            h.Travailler();
            h.Manger();
            h.Dormir();

            Robot r = new Robot("DG4000");
            r.Travailler();
            
        }
    }
}
