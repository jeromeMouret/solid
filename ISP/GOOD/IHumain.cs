﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISP
{
    public interface IHumain
    {
        void Manger();
        void Dormir();
    }
}