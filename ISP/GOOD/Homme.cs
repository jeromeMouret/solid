﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISP
{
    public class Homme : IHumain, IEmployer
    {
        public Homme(string Nom)
        {
            this.Nom = Nom;

        }

        public string Nom
        {
            get => default(string);
            set
            {
            }
        }

        public void Dormir()
        {
            Console.WriteLine("Je suis un homme et je dors.");
        }

        public void Manger()
        {
            Console.WriteLine("Je suis un homme et je mange.");
        }

        public void Travailler()
        {
            Console.WriteLine("Je suis un homme et je travaille.");
        }
    }
}