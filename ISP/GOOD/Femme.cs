﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISP
{
    public class Femme : IHumain, IEmployer
    {
        public Femme(string Nom)
        {
            this.Nom = Nom;
        }

        public string Nom
        {
            get => default(string);
            set
            {
            }
        }

        public void Dormir()
        {
            Console.WriteLine("Je suis une femme et je dors.");
        }


        public void Manger()
        {
            Console.WriteLine("Je suis une femme et je mange.");
        }

        public void Travailler()
        {
            Console.WriteLine("Je suis une femme et je travaille.");
        }
    }
}