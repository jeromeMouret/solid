﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRP.GOOD
{
    class Program
    {
        public static void Main(string[] args)
        {
            StudentData sData = new StudentData("Mouret","Jerome",DateTime.Now,1);
            StudentDAO sDAO = new StudentDAO();
            sDAO.Insert(sData);
            sDAO.Update(sData);

            StudentBusiness sBusiness = new StudentBusiness();
            sBusiness.CalculateAverage(sData);
            sBusiness.IsGraduated(sData);
        }
    }
}
