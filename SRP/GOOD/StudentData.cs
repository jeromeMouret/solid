﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRP
{
    public class StudentData
    {
        public StudentData(string FirstName, string LastName, DateTime BirthDay, int StudentID)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.BirthDay = BirthDay;
            this.StudentID = StudentID;

        }

        public string FirstName
        {
            get => default(string);
            set
            {
            }
        }

        public string LastName
        {
            get => default(string);
            set
            {
            }
        }

        public DateTime BirthDay
        {
            get => default(DateTime);
            set
            {
            }
        }

        public int StudentID
        {
            get => default(int);
            set
            {
            }
        }
    }
}