﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OCP
{
    public class DBLogger : ILogger
    {

        public void WriteLog(string message)
        {
            Console.WriteLine(message);
        }
    }
}