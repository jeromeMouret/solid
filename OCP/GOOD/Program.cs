﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OCP.GOOD
{
    class Program
    {
        public static void Main(string[] args)
        {

            DBLogger log = new DBLogger();
            log.WriteLog("Log dans une BDD");

            FileLogger logg = new FileLogger();
            logg.WriteLog("log dans un fichier");
        }

    }
}
