﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OCP
{
    public interface ILogger
    {
        void WriteLog(string message);
    }
}